$(document).ready(function(){ // Cela nous permet de charger tte la page

   $('#submit').click(function(){  /*function anonyme pas besoin de retour c'est comme une méthode*/

    //recupération des données
    let nom = $('#nom').val().trim(); // le "trim" est la afin que l'espace ne soit pas considéré comme une lettre
    let prenom = $('#prenom').val().trim();
    let email = $('#email').val().trim();
    let message = $('#message').val().trim();

    let chaine = $("form").serializeArray(); // cela nous permet d'avoir la requête sous forme de tableau
    console.log(chaine);

    if(nom == '' || prenom == ''){ 
        // alert('remplir tous les champs');

        $('#prenom').after('<span> Merci de remplir ce champs </span>'); // Lorsqu'on click et le champs et vide ce message s'affiche
        
        return false; // "false" c'est comme si l'évènement avait échoué ici il ne continue pas en bas
    }

    if (nom.length === 0){

    }

    $.ajax({

        type: "POST",           //où on veut récupérer les données
        url: "submission.php",
        data:{
            name : nom,
            firstName: prenom,
            mail: email,
            text: message

        },
        cache: false,  // ici on utlise pas le cache
        success: function(response){
            alert(response);
        },

        error: function(xhr,status,error){
            console.error(xhr);
        },

    });


   });

})